import sqlite3
from sqlite3 import Error
from processPB import *
from processPC import *
from processPD import *

def functionPA(filename):
    result = []
    fin = open(filename, 'r')
    #Read queries ...
    print('Reading queries ...')
    queries = []
    for line in fin:
        line = line.strip()
        queries.append(line)
    fin.close()
    #Process queries ...
    print('Processing queries ...')
    for query in queries:
        result.append(functionPB(query))
    #Do average ...
    print('Doing average ...')
    averageSalaries = functionPC(result)
    #Print results ...
    print('Printing results ...')
    for entry in averageSalaries:
        print('Age: ' + str(entry[0]) + ' -> Average Salary: ' + str(entry[1]) + '\n')
    #Get user's feedback ...
    print('Getting feedback ...')
    functionPD()
    return 'Finished!'

print(functionPA('Queries.txt'))
