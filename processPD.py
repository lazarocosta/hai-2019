from decimal import Decimal
import time

def functionPD():
    print('Please modify UserCommand.txt to change the way of showing results!')
    print('Please put:\n\t 0: for FETCHALL\n\t 1: for FETONE\n\t 2: for LIMIT 5\n\t n: for LIMIT n (n>2)\n')
    #nb = raw_input('Would you like to change the way of showing results?[y/n]')
    #if(str(nb) == 'y' or str(nb) == 'Y'):
    #    print('Please put:\n\t 0: for FETCHALL\n\t 1: for FETONE\n\t 2: for LIMIT 5\n\t n: for LIMIT n (n>2)\n'
    #    raw_input('Finish? Press any key to continue ...')
    #else:
    #    return
    fin = open('UserCommand.txt', 'r')
    fout = open('Command.txt', 'w')
    line = fin.readline()
    line = line.strip()
    num = int(line)
    if (num == 0):
        fout.write('FETCHALL')
    elif (num  == 1):
        fout.write('FETONE')
    elif (num == 2):
        fout.write('LIMIT 5')
    else:
        fout.write(str(num))
    fin.close()
    fout.close()
    return
