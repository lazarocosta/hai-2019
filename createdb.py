import sqlite3
import re
from sqlite3 import Error

def createdb():
	try:
		conn = sqlite3.connect('master.db')
		print('Opened database successfully')
		createQuery = '''Create table company
			(ID	INT PRIMARY KEY	NOT NULL,
			NAME	TEXT		NOT NULL,
			AGE	INT		NOT NULL,
			ADDRESS	CHAR(50),
			SALARY	REAL);'''
		conn.execute(createQuery)

		print('Table created sucessfully')
		conn.close()
		print('Closed database successfully')
	except Error as e:
		print(e)

def generatedata():
	try:
		conn = sqlite3.connect('master.db')
		print('Open database successfully')
		
		fin = open('RawData.txt')
		ID = 1
		for line in fin:
			items = line.split(" ")
			insertQuery = 'INSERT INTO company(ID, NAME, AGE, ADDRESS, SALARY) VALUES (' \
				+ str(ID) + ', \'' + items[0] + '\', ' \
				+ items[1] + ', \'' + items[2] + '\', ' + items[3].strip() + ')'
			#print insertQuery
			ID = ID + 1
			conn.execute(insertQuery)
		fin.close()
		conn.commit()
		print('Records created successfully')
		conn.close()
		print('Database closed')
	except Error as e:
		print(e) 

def testdb():
	try:
		conn = sqlite3.connect('master.db')
		print('Open database successfully')

		cursor = conn.execute('Select id, name, address, salary from company')

		for row in cursor:
			print(row)
		print('Operation done successfully')
		conn.close()
		print('Database closed')
	except Error as e:
		print(e)

if __name__ == '__main__':
	print('creating db and table...')
	createdb();
	print('inserting data...')
	generatedata();
	print('testing...')
	testdb();
