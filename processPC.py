from decimal import Decimal
import time

def functionPC(data):
    results = []
    if(data == None):
        return results
    age = -1
    count = 0
    sumsal = -1.0
    for rows in data:
        age = -1
        count = 0
        sumsal = -1.0
        for row in rows:
            if(row == None):
                continue
            if(age < 0):
                age = int(row[2])
                sumsal = float(row[4])
                count = 1
            else:
                sumsal += float(row[4])
                count += 1
        #Add to the list ...
        if(sumsal >= 0):
            results.append([age, sumsal/count])
    time.sleep(1)
    return results
